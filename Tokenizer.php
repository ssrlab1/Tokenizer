<?php
	class Tokenizer
	{
		private static $localizationArr = array();
		private static $localizationErr = array();
		private $text = '';
		private $language = '';
		private $alphabet = '';
		private $characterArr = array();
		private $resultArr = array();

		private $letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдежзийклмнопрстуфхцчшщъыьэюяЂЃѓЉЊЌЋЏђљњќћџЎўЈҐЁЄЇІіґёєјЅѕї';
		private $digits = '0123456789';
		private $whitespaces = " \r\n";
		private $lettersBe = 'АБВГДЕЁЖЗІЙКЛМНОПРСТУЎФХЦЧШЫЬЭЮЯабвгдеёжзійклмнопрстуўфхцчшыьэюя';
		private $lettersEn = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
		private $lettersRu = 'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя';
		
		private $patternsArr = array();
		private $stringResult1 = '';
		
		const INPUT_TEXT_DEFAULT_BE = "Сі́рыус (лац. Sirius), таксама Альфа Вялікага Сабакі (лац. Alpha Canis Majoris) – найярчэйшая зорка начного неба. Бачная зорная велічыня -1,46. Сірыус аддалены ад Сонца на 8,60±0,04 св. гадоў і з'яўляецца адной з бліжэйшых да нас зорак. Нябесныя каардынаты: прамое ўзыходжанне 6 гадз. 45 хв. 9 сек., схіленне -16°42'58''. Назва паходзіць з грэцкай мовы і азначае \"бліскучы\" альбо \"пякучы\".\n\nСкарына (3283 Skorina, 1979 QA10) – астэроід № 3283. Належыць да Галоўнага пояса астэроідаў Сонечнай сістэмы. Дыяметр – 12,65 км. Сярэдняя аддаленасць астэроіда ад Сонца складае 2,396 астранамічнай адзінкі (~ 360 млн. км.). Перыяд поўнага абарачэння вакол Сонца – 3,71 зямнога года. Адкрыты астраномам Мікалаем Іванавічам Чарных у Крымскай астрафізічнай абсерваторыі 27 жніўня 1979 г. Названы ў гонар беларускага першадрукара Францыска Скарыны.";
		const INPUT_TEXT_DEFAULT_RU = "Я= вздохну+л, положи+л перо+ и= при+нялся жда+ть господи+на сЪкока+рдой. Начина+ющие писа+тели и= вообще+ лю+ди, неЪпосвящё+нные вЪредакцио+нные та+йны, приходя+щие приЪсло+ве реда+кция вЪсвящё+нный тре+пет, заставля+ют жда+ть себя= нема+лое вре+мя. Они=, по+сле реда+кторского проси+ , до+лго ка+шляют, до+лго сморка+ются, ме+дленно отворя+ют две+рь, ещё= ме+дленнее вхо+дят и= э=тим отнима+ют нема=ло вре+мени. Господи+нЪже сЪкока+рдой неЪзаста+вил жда+ть себя=. НеЪуспе+ла заЪАндре+ем затвори+ться две+рь, ка=к я= уви+дел вЪсвоё+м кабине+те высо+кого широкопле+чего мужчи+ну, держа+вшего вЪодно=й руке+ бума+жный свё+рток, а= вЪдруго+й - фура+жку сЪкока+рдой. приве+т, приве+т. приве+т: приве+т - приве+т, - приве+т; приве+т и= приве+т приве+т и=ли приве+т приве+т, а= приве+т. приве+т? приве+т приве+т? приве+т! приве+т приве+т! приве+т приве+т. приве+т (приве+т приве+т приве+т приве+т приве+т приве+т приве+т приве+т приве+т приве+т приве+т приве+т).";
		const BR = "<br>\n";

		function __construct()
		{

		}

		public static function loadLanguages()
		{
			$languages = array();
			$files = scandir('lang/');
			if(!empty($files))
			{
				foreach($files as $file)
				{
					if(substr($file, 2, 4) == '.txt')
					{
						$languages[] = substr($file, 0, 2);
					}
				}
			}
			return $languages;
		}
		
		public static function loadLocalization($lang)
		{
			$filepath = "lang/$lang.txt";
			$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			if($linesArr === false)
			{
				$filepath = "lang/en.txt";
				$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			}
			foreach($linesArr as $line)
			{
				if(empty($line))
				{
					$key = $value = '';
				}
				elseif(substr($line, 0, 1) !== '#' && substr($line, 0, 2) !== '//')
				{
					if(empty($key))
					{
						$key = $line;
					}
					else
					{
						if(!isset(self::$localizationArr[$key]))
						{
							self::$localizationArr[$key] = $line;
						}
					}
				}
			}
		}
		
		public static function showMessage($msg)
		{
			if(isset(self::$localizationArr[$msg]))
			{
				return self::$localizationArr[$msg];
			}
			else
			{
				self::$localizationErr[] = $msg;
				return $msg;
			}
		}
		
		public static function sendErrorList($lang)
		{
			if(!empty(self::$localizationErr))
			{
				$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
				$sendersName = 'Tokenizer';
				$recipient = 'corpus.by@gmail.com';
				$subject = "ERROR: Incorrect localization in $sendersName by user $ip";
				$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
				$mailBody .= $_SERVER['HTTP_HOST'] . '/Tokenizer/ дасылае інфармацыю аб наступных памылках:' . self::BR . self::BR;
				$mailBody .= "Мова лакалізацыі: <b>$lang</b>" . self::BR;
				$mailBody .= 'Адсутнічае лакалізацыя наступных ідэнтыфікатараў:' . self::BR . implode(self::BR, self::$localizationErr) . self::BR;
				$header  = "MIME-Version: 1.0\r\n";
				$header .= "Content-type: text/html; charset=utf-8\r\n";
				$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
				mail($recipient, $subject, $mailBody, $header);
			}
		}
		
		public function setText($text)
		{
			$this->text = $text;
		}
		
		public function setLanguage($language)
		{
			$this->language = $language;
			if($this->language == 'be') $this->alphabet = $this->lettersBe;
			if($this->language == 'en') $this->alphabet = $this->lettersEn;
			if($this->language == 'ru') $this->alphabet = $this->lettersRu;
		}

		public function run()
		{
			$this->characterArr = $characterArr = $this->characterClassification($this->text);
			$this->readPatterns();
			
			for($i = 0; $i < count($characterArr); $i++)
			{
				// Натрапілі на некласіфікаваны сімвал у спісе токенаў
				if(isset($characterArr[$i][1]) && $characterArr[$i][1] == 'character')
				{
					// Калі на такі сімвал ёсць правілы
					if(isset($this->patternsArr[$characterArr[$i][0]]))
					{
						// Перабіраем правілы на дадзены сімвал
						foreach($this->patternsArr[$characterArr[$i][0]] as $rule => $class)
						{
							$ruleArr = explode('_', $rule);
							$tmp = $characterIndex = array_search('character', $ruleArr);
							$lastIndex = count($ruleArr) - 1;
							
							$isAppropriateRule = true;
							$newToken = '';
							for($j = 0; $j <= $lastIndex; $j++)
							{
								if(isset($characterArr[$i-$tmp]))
								{
									$newToken .= $characterArr[$i-$tmp][0];
									if($characterArr[$i-$tmp][1] == $ruleArr[$j])
									{
										$tmp--;
									}
									elseif($characterArr[$i-$tmp][1] . '(' . $characterArr[$i-$tmp][0] . ')' == $ruleArr[$j])
									{
										$tmp--;
									}
									else
									{
										$isAppropriateRule = false;
										break;
									}
								}
								else
								{
									$isAppropriateRule = false;
									break;
								}
							}
							
							$tmp = $characterIndex;
							if($isAppropriateRule)
							{
								for($j = 0; $j < $lastIndex; $j++)
								{
									$tmp--;
									unset($characterArr[$i-$tmp]);
								}
								$characterArr[$i-$characterIndex] = array($newToken, $class, mb_strlen($newToken, 'UTF-8'));
							}
						}
					}
				}
			}
			$this->resultArr = $characterArr = array_values($characterArr);
		}
		
		private function readPatterns()
		{
			$filePath = dirname(__FILE__) . '/rules/patterns.txt';
			$handle = fopen($filePath, 'r') OR die("fail open 'patterns.txt'");
			if($handle)
			{
				$character = '';
				while(($buffer = fgets($handle, 4096)) !== false)
				{
					if(!empty($buffer) && substr($buffer, 0, 1) != '#')
					{
						if(mb_substr($buffer, 0, 10, 'UTF-8') == 'character=')
						{
							$character = mb_substr($buffer, 10, 1, 'UTF-8');
						}
						else
						{
							$patternsArr = preg_split("/\t/", $buffer);
							if(isset($patternsArr[0]) && isset($patternsArr[1]))
							{
								$this->patternsArr[$character][$patternsArr[0]] = $patternsArr[1];
							}
						}
					}
				}
			}
			fclose($handle);
		}
		
		private function characterClassification()
		{
			$tokensArr = array();
			
			$tokenCur = '';
			$tokenCnt = 0;
			$charTypeCur = '';
			$cntCur = 0;
			
			$tokensDelimiter = ',';
			$thousandCharactersArr = $this->strSplitUnicode($this->text, 1024);
			foreach($thousandCharactersArr as $thousandCharacters)
			{
				$chars = $this->strSplitUnicode($thousandCharacters);
				foreach($chars as $char)
				{
					if(strpos($this->alphabet, $char) !== false) $charType = 'alphabet';
					elseif(strpos($this->letters, $char) !== false) $charType = 'letters';
					elseif(strpos($this->digits, $char) !== false) $charType = 'digits';
					elseif(strpos($this->whitespaces, $char) !== false) $charType = 'whitespaces';
					else $charType = 'character';
										
					if($charType !== 'character')
					{
						if($charType == $charTypeCur)
						{
							$tokenCur .= $char;
							$cntCur++;
							continue;
						}
						else
						{
							if(!empty($charTypeCur))
							{
								$this->stringResult1 .= $tokenCnt . '_' . $charTypeCur . '_' . $cntCur . $tokensDelimiter;
								$tokensArr[$tokenCnt] = array($tokenCur, $charTypeCur, $cntCur);
								$tokenCnt++;
							}
							$tokenCur = $char;
							$cntCur = 1;
						}
						$charTypeCur = $charType;
					}
					else
					{
						$this->stringResult1 .= $tokenCnt . '_' . $charTypeCur . '_' . $cntCur . $tokensDelimiter;
						$tokensArr[$tokenCnt] = array($tokenCur, $charTypeCur, $cntCur);
						$tokenCnt++;
						$charTypeCur = 'character';
						$tokenCur = $char;
						$cntCur = 1;
					}
				}
			}
			$this->stringResult1 .= $tokenCnt . '_' . $charTypeCur . '_' . $cntCur . $tokensDelimiter;
			$tokensArr[$tokenCnt] = array($tokenCur, $charTypeCur, $cntCur);
			return $tokensArr;
		}
		
		private function strSplitUnicode($str, $length = 0)
		{
			if($length > 0)
			{
				$resultArr = array();
				$len = mb_strlen($str, 'UTF-8');
				for($i = 0; $i < $len; $i += $length)
				{
					$resultArr[] = mb_substr($str, $i, $length, 'UTF-8');
				}
				return $resultArr;
			}
			return preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);
		}
		
		public function getCharacterArr() { return $this->characterArr; }
		public function getResultArr() { return $this->resultArr; }
	}
?>