<?php
	header("Content-Type: text/html; charset=utf-8");
	$ini = parse_ini_file('service.ini');
	include_once 'Tokenizer.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	$languages = Tokenizer::loadLanguages();
	Tokenizer::loadLocalization($lang);
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
	<head>
		<title><?php echo Tokenizer::showMessage('title'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href='css/theme.css'>
		<link rel="icon" type="image/x-icon" href="img/favicon.ico">
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
		<?php include_once 'analyticstracking.php'; ?>
		<script>
			var inputTextDefault = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', Tokenizer::showMessage('default input'))); ?>";
			$(document).ready(function () {
				$('button#MainButtonId').click(function() {
					$('#resultBlockId').show('slow');
					$('#beforeProcessingId').empty();
					$('#beforeProcessingId').prepend($('<img>', { src: "img/loading.gif"}));
					$('#afterProcessingId').empty();
					$('#afterProcessingId').prepend($('<img>', { src: "img/loading.gif"}));
					$.ajax({
						type: 'POST',
						url: 'https://corpus.by/Tokenizer/api.php',
						data: {
							'localization': '<?php echo $lang; ?>',
							'inputText': $('textarea#inputTextId').val(),
							'language': $('select#languageSelect').val()
						},
						success: function(msg){
							var result = jQuery.parseJSON(msg);
							var beforeProcessing = '<table><tbody>';
							for (var token in result.CharacterArr) {
								if (typeof result.CharacterArr[token] !== 'function') {
									beforeProcessing += '<tr><td width=150>' + result.CharacterArr[token][0] + '</td><td width=150>' + result.CharacterArr[token][1] + '</td><td width=150>' + result.CharacterArr[token][2] + '</td></tr>';
								}
							}
							beforeProcessing += '</tbody></table>';
							$('#beforeProcessingId').html(beforeProcessing);
							var afterProcessing = '<table><tbody>';
							for (var token in result.ResultArr) {
								if (typeof result.ResultArr[token] !== 'function') {
									afterProcessing += '<tr><td width=150>' + result.ResultArr[token][0] + '</td><td width=150>' + result.ResultArr[token][1] + '</td><td width=150>' + result.ResultArr[token][2] + '</td></tr>';
								}
							}
							afterProcessing += '</tbody></table>';
							$('#afterProcessingId').html(afterProcessing);
						},
						error: function(){
							$('#resultId').html('ERROR');
						}
					});
				});
			});
		</script>
	</head>
	<body>
		<!-- Novigation -->
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/"><img style="max-width:150px; margin-top: -7px;" src="img/main-logo.png" alt=""></a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="service-name"><a href="/Tokenizer/?lang=<?php echo $lang; ?>"><?php echo Tokenizer::showMessage('title'); ?></a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="service-name"><a href="<?php echo Tokenizer::showMessage('help'); ?>" target="_blank">?</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo Tokenizer::showMessage($lang); ?><span class="caret"></span></a>
							<ul class="dropdown-menu">
								<?php
									$languages = Tokenizer::loadLanguages();
									foreach($languages as $language) {
										echo "<li><a href='?lang=$language'>" . Tokenizer::showMessage($language) . "</a></li>";
									}
								?>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- End of Novigation -->
		<div class="container theme-showcase" role="main">
			<div class="row">
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value=inputTextDefault;"><?php echo Tokenizer::showMessage('refresh'); ?></button>
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value='';"><?php echo Tokenizer::showMessage('clear'); ?></button>
								</div>
								<h3 class="panel-title"><?php echo Tokenizer::showMessage('input'); ?></h3>
							</div>
							<div class="panel-body">
								<p><textarea class="form-control" rows="10" id = "inputTextId" name="inputText" placeholder="Enter text"><?php echo str_replace('\n', "\n", Tokenizer::showMessage('default input')); ?></textarea></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="control-panel">
						<span class="bold"><?php echo Tokenizer::showMessage('language selector'); ?>&nbsp;</span>
						<select name='language' id="languageSelect" class="selector-primary">
							<option value='be'><?php echo Tokenizer::showMessage('belarusian'); ?></option>
							<option value='en'><?php echo Tokenizer::showMessage('english'); ?></option>
							<option value='ru'><?php echo Tokenizer::showMessage('russian'); ?></option>
						</select>
						<button type="button" id="MainButtonId" name="MainButton" class="button-primary"><?php echo Tokenizer::showMessage('button'); ?></button>
					</div>
				</div>
				<div class="col-md-12" id="resultBlockId" style="display: none;">
					<div class="col-md-6">
						<div class="control-panel">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title"><?php echo Tokenizer::showMessage('before processing'); ?></h3>
								</div>
								<div class="panel-body">
									<p id="beforeProcessingId"></p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="control-panel">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title"><?php echo Tokenizer::showMessage('after processing'); ?></h3>
								</div>
								<div class="panel-body">
									<p id="afterProcessingId"></p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-body">
								<?php echo Tokenizer::showMessage('service code'); ?>&nbsp;<a href="https://gitlab.com/ssrlab1/Tokenizer" target="_blank"><?php echo Tokenizer::showMessage('reference'); ?></a>.
								<br />
								<a href="https://gitlab.com/ssrlab1/Tokenizer/-/issues/new" target="_blank"><?php echo Tokenizer::showMessage('suggestions'); ?></a>.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="footer">
			<div class="container">
				<p class="text-muted">
					<?php echo Tokenizer::showMessage('contact e-mail'); ?>
					<a href="mailto:corpus.by@gmail.com">corpus.by@gmail.com</a>.<br />
					<?php echo Tokenizer::showMessage('other prototypes'); ?>
					<a href="https://corpus.by/?lang=<?php echo $lang; ?>">corpus.by</a>,&nbsp;<a href="https://ssrlab.by">ssrlab.by</a>.
				</p>
				<p class="text-muted">
					<?php echo Tokenizer::showMessage('laboratory'), ', ', $ini['year']; if($ini['year'] !== date('Y')) echo '—', date('Y'); ?>
				</p>
			</div>
		</footer>
	</body>
</html>
<?php Tokenizer::sendErrorList($lang); ?>