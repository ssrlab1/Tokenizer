<?php
	header("Content-type: text/html;  charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
		
	$localization = isset($_POST['localization']) ? $_POST['localization'] : 'en';
	$text = isset($_POST['inputText']) ? $_POST['inputText'] : '';
	$language = isset($_POST['language']) ? $_POST['language'] : 'be';
	
	include_once 'Tokenizer.php';
	Tokenizer::loadLocalization($localization);
	
	$msg = '';
	if(!empty($text))
	{
		$Tokenizer = new Tokenizer();
        $Tokenizer->setText($text);
        $Tokenizer->setLanguage($language);
        $Tokenizer->run();
		
		$result['text'] = $text;
		$result['CharacterArr'] = $Tokenizer->getCharacterArr();
        $result['ResultArr'] = $Tokenizer->getResultArr();
		$msg = json_encode($result);
	}
	echo $msg;
?>